#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>
#include <thread>
#include <chrono>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <zmq.hpp>


#define BUFFER_LENGTH 20000               ///< The buffer length (crude but fine)
static char receive[BUFFER_LENGTH];     ///< The receive buffer from the LKM
 
int main(int argc, char * argv[]){
	int ret, fd;
	//  Prepare our context and socket

	zmq::context_t context(1);
    zmq::socket_t subscriber (context, ZMQ_SUB);

    //  Initialize random number generator
    srandom ((unsigned) time (NULL));

    if (argc == 2)
        subscriber.connect(argv [1]);
    else
        subscriber.connect("tcp://localhost:5556");

	std::stringstream ss;
    ss << std::dec << std::setw(3) << std::setfill('0');
    std::cout << "topic:" << ss.str() << std::endl;

	subscriber.setsockopt( ZMQ_SUBSCRIBE, ss.str().c_str(), ss.str().size());

	printf("Reading from the device...\n");

	while (1) {
		int data = zmq_recv((void*)subscriber, receive, BUFFER_LENGTH-1, 0);
        
        std::cout << data << "<==>" << receive << std::endl;
        memset(receive, 0, BUFFER_LENGTH);
    }
	return 0;
}

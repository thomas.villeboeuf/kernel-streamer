cmake_minimum_required( VERSION 3.6 )
project (kn-consumer)

set(CMAKE_BUILD_TYPE Release)

## use this to globally use C++11 with in our project
set(CMAKE_CXX_STANDARD 11)

add_subdirectory(src)
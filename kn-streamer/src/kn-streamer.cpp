/**
 * @file   testebbchar.c
 * @author Derek Molloy
 * @date   7 April 2015
 * @version 0.1
 * @brief  A Linux user space program that communicates with the ebbchar.c LKM. It passes a
 * string to the LKM and reads the response from the LKM. For this example to work the device
 * must be called /dev/ebbchar.
 * @see http://www.derekmolloy.ie/ for a full description and follow-up descriptions.
*/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <pthread.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <linux/ioctl.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include <zmq.hpp>

#define MY_IOCTL_DATA_TO_READ _IOC(_IOC_READ, 'k', 1, sizeof(unsigned short))

#define BUFFER_LENGTH 20000               ///< The buffer length (crude but fine)
static char receive[BUFFER_LENGTH];     ///< The receive buffer from the LKM
static const std::string addr = "tcp://*:5556";

int main(int argc, char * argv[]){
	int ret, fd;
	char stringToSend[BUFFER_LENGTH];
	uint16_t value;
	//  Prepare our context and socket
	zmq::context_t context(1);
    zmq::socket_t publisher(context, ZMQ_PUB);

	if (argc == 2)
        publisher.bind(argv [1]);
    else
		try 
		{
			publisher.bind(addr);
		}
		catch (zmq::error_t &ex) 
		{
			std::cout << ex.num() << std::endl;

			if (ex.num() == 98) 	//Error str "Address already in use"
			{
				throw;
			}
			else if (ex.num() != EINTR) 
			{
				throw;
			}
		}

	printf("Starting device test code example...\n");
	fd = open("/dev/stream0", O_RDWR);             // Open the device with read/write access
	if (fd < 0){
		perror("Failed to open the device...");
		return errno;
	}

	printf("Reading from the device...\n");
	while (1)
	{
		if(ioctl(fd, MY_IOCTL_DATA_TO_READ, (unsigned long *) &value) && (value >0))
		{
			printf("IOCTL read error\n");
			break;
		}

		if(value > 0)
		{
			printf("Value is %d\n", value);
			ret = read(fd, receive, value);        // Read the response from the LKM
			if(ret < 0){
				perror("Failed to read the message from the device.");
				return errno;
			}
			if(strlen(receive) > 0)
			{
				printf("The received message is: [%s]\n", receive);
				printf("ZmQ publish\n");
				zmq_send((void*)publisher, receive, value, 0);
				memset(receive, 0, BUFFER_LENGTH);
			}
		}         
	}   

	return 0;
}
# kernel-streamer

## Source website driver
* https://www.apriorit.com/dev-blog/195-simple-driver-for-linux-os
* https://linux-kernel-labs.github.io/refs/heads/master/labs/device_drivers.html
* https://olegkutkov.me/2020/02/10/linux-block-device-driver/
* http://rkalimuthu1987.blogspot.com/2014/04/simple-char-driver-and-user-space.html
* http://embeddedguruji.blogspot.com/2019/01/linux-character-driver-creating.html
* 

## Source website ioctl
* https://linux-kernel-labs.github.io/refs/heads/master/labs/device_drivers.html#ioctl
* http://www.cs.otago.ac.nz/cosc440/labs/lab06.pdf
* https://embetronicx.com/tutorials/linux/device-drivers/ioctl-tutorial-in-linux/

## Source website ZMQ
* https://brettviren.github.io/cppzmq-tour/index.html#intro


## Data flow in this example
4 sub-module are in place to have the full use-case.

### 1. kn-data-stream
It's the driver, in kernel-space. 
It's an `Character device drivers`, so the file operation are available (open, close, read, write and ioctl).

### 2. kn-streamer
It's an user-space app.
In one side, it's the unique module which communicate with the kernel driver using file operation.
And in other side, stream data using ZeroMQ lib as PUBLISHER.
Now anyone (other app in user-space) can subscribe to get the data stream.

### 3. kn-consumer
It's an user-space app.
It's one of the SUBSCRIBER of the `kn-streamer`. In the futur, it will be the template of algo-app.

### 4. kn-user-app (simulator)
It's an module to simulate data incoming to driver.